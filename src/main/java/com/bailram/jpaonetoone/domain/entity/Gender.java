package com.bailram.jpaonetoone.domain.entity;

public enum Gender {
    MALE,
    FEMALE
}
