package com.bailram.jpaonetoone.repository;

import com.bailram.jpaonetoone.domain.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
}
