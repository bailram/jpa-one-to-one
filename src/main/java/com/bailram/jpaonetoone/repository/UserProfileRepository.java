package com.bailram.jpaonetoone.repository;

import com.bailram.jpaonetoone.domain.entity.UserProfile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserProfileRepository extends JpaRepository<UserProfile, Long> {
}
