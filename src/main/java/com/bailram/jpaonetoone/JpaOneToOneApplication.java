package com.bailram.jpaonetoone;

import com.bailram.jpaonetoone.domain.entity.Gender;
import com.bailram.jpaonetoone.domain.entity.User;
import com.bailram.jpaonetoone.domain.entity.UserProfile;
import com.bailram.jpaonetoone.repository.UserProfileRepository;
import com.bailram.jpaonetoone.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Calendar;

@SpringBootApplication
public class JpaOneToOneApplication implements CommandLineRunner {
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserProfileRepository userProfileRepository;

	public static void main(String[] args) {
		SpringApplication.run(JpaOneToOneApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// Clean up database tables
		userProfileRepository.deleteAllInBatch();
		userRepository.deleteAllInBatch();

		// Create a User instance
		User user = User.builder()
				.firstName("Dhimas")
				.lastName("Bayu")
				.email("dhimas@gmail.com")
				.password("dhimasganteng")
				.build();

		Calendar dateOfBirth = Calendar.getInstance();
		dateOfBirth.set(1999, 1, 11);

		// Create a UserProfile instance
		UserProfile userProfile = UserProfile.builder()
				.phoneNumber("+62822-5775-2251")
				.gender(Gender.MALE)
				.dateOfBirth(dateOfBirth.getTime())
				.address1("Kost aan")
				.address2("Karanglo")
				.street("Tamanan")
				.city("Banguntapan")
				.state("Yogyakarta")
				.country("Indonesia")
				.zipCode("65181")
				.build();

		// Set child reference(userProfile) in parent entity(user)
		user.setUserProfile(userProfile);

		// Set parent reference(user) in child entity(userProfile)
		userProfile.setUser(user);

		// Save parent Reference (which will save the child as well)
		userRepository.save(user);

	}
}
